
import { SkElement } from '../../sk-core/src/sk-element.js';


export class SkSpinner extends SkElement {

    get cnSuffix() {
        return 'spinner';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }

    get display() {
        return this.getAttribute('display') || 'none';
    }

    set display(display) {
        this.setAttribute('display', display);
        if (display === 'none') {
            this.hide();
        } else {
            this.show();
        }
    }

    show() {
        this.style.display = 'inline-block';
    }

    hide() {
        this.style.display = 'none';
    }

    toggle() {
        if (this.display === 'none') {
            this.display = 'inline-block';
            this.show();
        } else {
            this.display = 'none';
            this.hide();
        }
    }

    attributeChangedCallback(name, oldValue, newValue) {
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
    }

    static get observedAttributes() {
        return ['display'];
    }
}
