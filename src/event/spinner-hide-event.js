

export const SPINNER_HIDE_EVT = 'spinner-hide';

export class SpinnerHideEvent extends CustomEvent {
    constructor(options) {
        super(SPINNER_HIDE_EVT, options);
    }
}
