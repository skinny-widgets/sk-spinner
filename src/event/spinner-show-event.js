

export const SPINNER_SHOW_EVT = 'spinner-show';

export class SpinnerShowEvent extends CustomEvent {
    constructor(options) {
        super(SPINNER_SHOW_EVT, options);
    }
}
