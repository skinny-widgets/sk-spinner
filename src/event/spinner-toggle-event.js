

export const SPINNER_TOGGLE_EVT = 'spinner-toggle';

export class SpinnerToggleEvent extends CustomEvent {
    constructor(options) {
        super(SPINNER_TOGGLE_EVT, options);
    }
}
