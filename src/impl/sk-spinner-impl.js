
import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';
import { SPINNER_SHOW_EVT } from "../event/spinner-show-event.js";
import { SPINNER_HIDE_EVT } from "../event/spinner-hide-event.js";
import { SPINNER_TOGGLE_EVT } from "../event/spinner-toggle-event.js";


export class SkSpinnerImpl extends SkComponentImpl {

    get suffix() {
        return 'spinner';
    }

    bindEvents() {
        if (this.showHandle) {
            this.comp.removeEventListener('show', this.showHandle);
            this.comp.removeEventListener(SPINNER_SHOW_EVT, this.showHandle);
        }
        this.showHandle = this.comp.show.bind(this.comp);
        this.comp.addEventListener('show', this.showHandle);
        this.comp.addEventListener(SPINNER_SHOW_EVT, this.showHandle);
        if (this.hideHandle) {
            this.comp.removeEventListener('hide', this.hideHandle);
            this.comp.removeEventListener(SPINNER_HIDE_EVT, this.hideHandle);
        }
        this.hideHandle = this.comp.hide.bind(this.comp);
        this.comp.addEventListener('hide', this.hideHandle);
        this.comp.addEventListener(SPINNER_HIDE_EVT, this.showHandle);
        if (this.toggleHandle) {
            this.comp.removeEventListener('toggle', this.toggleHandle);
            this.comp.removeEventListener(SPINNER_TOGGLE_EVT, this.toggleHandle);
        }
        this.toggleHandle = this.comp.toggle.bind(this.comp);
        this.comp.addEventListener('toggle', this.toggleHandle);
        this.comp.addEventListener(SPINNER_TOGGLE_EVT, this.toggleHandle);
    }

    unbindEvents() {
        this.comp.removeEventListener('show', this.showHandle);
        this.comp.removeEventListener(SPINNER_SHOW_EVT, this.showHandle);
        this.comp.removeEventListener('hide', this.hideHandle);
        this.comp.removeEventListener(SPINNER_HIDE_EVT, this.hideHandle);
        this.comp.removeEventListener('toggle', this.toggleHandle);
        this.comp.removeEventListener(SPINNER_TOGGLE_EVT, this.toggleHandle);
    }

}